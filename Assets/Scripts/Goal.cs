﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player") {
            // TODO: refactor. too much hardcoding, and loading scenes should be
            // handled in a single class rather than multiple
            SceneManager.LoadScene(0);
        }
    }
}
