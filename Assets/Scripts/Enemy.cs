﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    // there's an outer capsule collider on this game object that will
    // start moving once it spots the player in range
    Collider rangeCollider;
    public float speed = 200.0f;
    float step;
    GameObject target;
    Vector3 targetPosition;
    Animator animator;

    public int health = 1;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        target = gameObject;
        targetPosition = target.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        MoveTowardsTarget();
        CheckDeath();
        AttackPlayer();
	}


	public void EnemyAttackAnimationEnded() {
		Debug.Log("Enemy: Attack Animation ended");
        animator.SetBool("isAttacking", false);
	}


    public void AttackPlayer() {
        if (target != gameObject && !animator.GetBool("isAttacking") && Vector3.Distance(transform.position, target.transform.root.position) < 2.0f) {
            //Debug.Log(target.transform.root.name);
            animator.SetBool("isAttacking", true);

        }
    }


    void CheckDeath() {
        if (health < 1) {
            Destroy(gameObject);
        }
    }


    public void TakeDamage() {
        health -= 1;            
    }


    void MoveTowardsTarget() {
        if (Vector3.Distance(transform.position, target.transform.root.position) > 1.0f) {
			step = speed * Time.deltaTime;
            // only make them follow in the x and z positions. dont want them flying
            // make them look towards target as well
            targetPosition = new Vector3(target.transform.root.position.x, transform.position.y, target.transform.root.position.z);
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);
            transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, targetPosition - transform.position, step, 0.0f));
        }
		
    }

	// this is the big range collider

    void OnTriggerEnter(Collider other) {
        // check the collision tag to see if its the player
		if (other.gameObject.tag == "Player") {
            //Debug.Log("Enemy: Found Target");
            // set the new target
            target = other.gameObject;
		}
	}
}
