﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGame : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        handleEsc();
	}

    void handleEsc() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            quit();
        }
    }

    public void quit() {
        print("Quitting the game");
        Application.Quit();

    }
}
