﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// for animation events
public class PlayerAnimations : MonoBehaviour {

    public GameObject player;
    Animator animator;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void AttackAnimationEnded()
	{
		Debug.Log("PlayerAnimation: Attack Animation ended");
        animator.SetBool("isAttacking", false);
	}

}
