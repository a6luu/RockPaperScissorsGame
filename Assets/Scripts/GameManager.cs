﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public GameObject player;

    public int totalKeys = 0;
    public int foundKeys = 0;
    public GameObject doorGO;
    public bool openDoor = false;

    Animator doorAnimator;


	private GUIStyle myGUIStyle;

	// Use this for initialization
	void Start () {
        myGUIStyle = new GUIStyle();
        myGUIStyle.fontSize = 20;
        myGUIStyle.fontStyle = FontStyle.Bold;
        myGUIStyle.border = new RectOffset(2, 2, 2, 2);
        myGUIStyle.normal.textColor = Color.white;
        findNumKeysInMap();

        doorAnimator = doorGO.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void findNumKeysInMap() {
        totalKeys = GameObject.FindGameObjectsWithTag("Key").Length;
    }

    public void updateFoundKeys() {
        foundKeys += 1;
        if (foundKeys == totalKeys && !openDoor) {
            doorAnimator.SetBool("OpenDoor", true);
        }
    }

	void OnGUI()
	{
        // TODO: make this nicer and cleaner
        GUI.Label(new Rect(10, 10, 100, 20), "Keys: " + foundKeys.ToString() + "/" + totalKeys.ToString(), myGUIStyle);
	}
}
