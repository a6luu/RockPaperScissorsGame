﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// this probs needs a refactor. dont like how its organized
// this class is attached to the enemy bone. It handles colliders here
public class EnemyBoneCollider : MonoBehaviour {

    GameObject parentEnemyGO;
    Animator parentEnemyAnimator;

	// Use this for initialization
	void Start () {
        parentEnemyGO = transform.root.gameObject;
        parentEnemyAnimator = parentEnemyGO.GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.root.tag == "Player" && parentEnemyAnimator.GetBool("isAttacking")) {
            GameObject playerGO = collision.transform.root.gameObject;
            Player player = playerGO.GetComponent<Player>();
            player.TakeDamage();
        }

    }
}
