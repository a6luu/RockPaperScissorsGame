﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Form {
    ROCK,
    PAPER,
    SCISSORS
}

public class Player : MonoBehaviour
{
    public Form currentForm = Form.ROCK;

    public GameObject gameManager;
    public GameObject rockGO;
    public GameObject paperGO;
    public GameObject scissorsGO;
    public GameObject rockBone;
    public GameObject paperBone;
    public GameObject scissorsBone;
    public bool isJumping = false;

    public int maxHealth = 3;
    public int health = 3;

    float movementSpeed = 5.0f;
    float cameraSpeed = 70.0f;

    private Animator myAnimator;
    private Collider myCollider;
    private Rigidbody myRigidbody;



    // Use this for initialization
    void Start()
    {
        GetFormAnimatorAndCollider();
        myRigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        HandleAttack();
        HandleDirection();
        HandleMovement();
        HandleFormChange();
        CheckDeath();

    }

    private void FixedUpdate() {
        HandleAirtime();
    }


    /* this handles air time
     * types of airtime:
     * fly (paper)
     * jump (rock and scissors)
    */
    void HandleAirtime() {
		if (Input.GetKey(KeyCode.Space)) {
			if (currentForm == Form.PAPER) {
                // paper can fly
                //transform.position = new Vector3(transform.position.x, transform.position.y + 1.0f, transform.position.z);
                myRigidbody.AddForce(Vector3.up * 50);

			} else {
                // Rock and Scissors
                // everyone else can jump a bit
                if (!isJumping) {
                    myRigidbody.AddForce(Vector3.up * 400);
                    isJumping = true;
                }
		    }
		}
    }


    void HandleAttack() {
        // Attack with Return button
        if (Input.GetKey(KeyCode.Return)) {
            // not using trigger cuz its too slow
            myAnimator.SetBool("isAttacking", true);
            //Debug.Log("Playing the attack animation");

        }
        // the animator in PlayerAnimations will handle turning it off
    }

    void GetFormAnimatorAndCollider() {
		// Need to set up the correct myAnimator and myCollider
		// the animator is in the game object
		// the collider is in the bone's game object.
		switch(currentForm) {
            case Form.ROCK:
                myAnimator = rockGO.transform.GetComponent<Animator>();
                myCollider = rockBone.transform.GetComponent<Collider>();
                break;
            case Form.PAPER:
                myAnimator = paperGO.transform.GetComponent<Animator>();
                myCollider = paperBone.transform.GetComponent<Collider>();
                break;
            case Form.SCISSORS:
                myAnimator = scissorsGO.transform.GetComponent<Animator>();
                myCollider = scissorsBone.transform.GetComponent<Collider>();
                break;
        }
    }


    void ResetState() {
		isJumping = false;
        myAnimator.SetBool("isAttacking", false);
    }


    void HandleFormChange() {
        if (Input.GetKey(KeyCode.Alpha1)) {
            ResetState();
            // switch to ROCK
            if (currentForm != Form.ROCK) {
                //Debug.Log("Form Change: Rock");
                // disable other GOs and turn on rockGO
                rockGO.gameObject.SetActive(true);
                paperGO.gameObject.SetActive(false);
                scissorsGO.gameObject.SetActive(false);
                currentForm = Form.ROCK;
                GetFormAnimatorAndCollider();
            }
		}
		else if (Input.GetKey(KeyCode.Alpha2)) {
            ResetState();
			// switch to PAPER
            if (currentForm != Form.PAPER) {
				//Debug.Log("Form Change: Paper");
				// disable other GOs and turn on paperGO
				rockGO.gameObject.SetActive(false);
				paperGO.gameObject.SetActive(true);
				scissorsGO.gameObject.SetActive(false); 
                currentForm = Form.PAPER;
				GetFormAnimatorAndCollider();
			}
		}
		else if (Input.GetKey(KeyCode.Alpha3)) {
            ResetState();
			// switch to SCISSORS
			if (currentForm != Form.SCISSORS) {
				//Debug.Log("Form Change: Scissors");
				// disable other GOs and turn on scissorsGO
				rockGO.gameObject.SetActive(false);
				paperGO.gameObject.SetActive(false);
				scissorsGO.gameObject.SetActive(true);
                currentForm = Form.SCISSORS;
				GetFormAnimatorAndCollider();
			}
        } else {
            return;
        }
    }


    void HandleDirection() {
		//if (Input.GetKey(KeyCode.I)) {
        //  // disabled for now
		//	// look up 
		//	transform.Rotate(-Vector3.right * cameraSpeed * Time.deltaTime);
		//}
		if (Input.GetKey(KeyCode.J)) {
			// look left
			transform.Rotate(-Vector3.up * cameraSpeed * Time.deltaTime);
		}
		//if (Input.GetKey(KeyCode.K)) {
		//	// disabled for now
		//	// look down
		//	transform.Rotate(Vector3.right * cameraSpeed * Time.deltaTime);
		//}
		if (Input.GetKey(KeyCode.L)) {
            // look right
            transform.Rotate(Vector3.up * cameraSpeed * Time.deltaTime);
		}
    }

    void HandleMovement() {
		if (Input.GetKey(KeyCode.W)) {
			// move up
			transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);
		}
        if (Input.GetKey(KeyCode.A)) {
            // move left
            transform.Translate(Vector3.left * movementSpeed * Time.deltaTime);
        }
		if (Input.GetKey(KeyCode.S)) {
			// move down
            transform.Translate(Vector3.back * movementSpeed * Time.deltaTime);
		}
        if (Input.GetKey(KeyCode.D)) {
            // move right
            transform.Translate(Vector3.right * movementSpeed * Time.deltaTime);
        }
			
    }


    void CheckDeath() {
        if (health < 1) {
            Destroy(gameObject);
            Debug.Log("Game Over");
        }
    }


    // Handles the rigid body colliders on the player
    private void OnCollisionEnter(Collision collision)
    {
        // if is jumping, and there's a collision, turn it off
        Debug.Log("RigidBody COllider");
        if (isJumping) {
            isJumping = false;
        }
        // check the collision tag to see if its a key
        if (collision.gameObject.tag == "Key") {
            // destroy the collision and update the number of collectables
            Destroy(collision.gameObject);
            gameManager.GetComponent<GameManager>().updateFoundKeys();
        }


        // check the collision tag to see if its an enemy
        // if isAttacking, then do damage
        if (collision.gameObject.tag == "Enemy" && myAnimator.GetBool("isAttacking")) {
            // need to pass the root game object
            AttackEnemy(collision.transform.root.gameObject);
        }
    }


    private void AttackEnemy(GameObject enemyGO) {
        Debug.Log("Player: Attack!");
        Enemy enemy = enemyGO.GetComponent<Enemy>();
        enemy.TakeDamage();
    }


    public void TakeDamage() {
        // some hack to prevent them from doing damage when you're attacking
        // player has priority
        if (!myAnimator.GetBool("isAttacking")) {
			health -= 1;
			// push the player back a bit    
		}
    }


	// GUI
	void OnGUI()
	{
		// TODO: make this nicer and cleaner
        GUI.Label(new Rect(200, 10, 100, 20), "Health: " + health.ToString() + "/" + maxHealth.ToString());
	}


}
