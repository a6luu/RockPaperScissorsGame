A 3D platformer.
Switch forms between a Rock, a Paper, or a pair of Scissors to collect the keys necessary to open the door to the next level.

Uses free assets from Unity Survival Shooter tutorial: https://unity3d.com/learn/tutorials/projects/survival-shooter-tutorial
Uses my own assets as well
